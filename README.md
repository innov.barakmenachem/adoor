# adoor

Flutter app to control automatic gate.

## State Managment
State managment is architecured with [flutter_redux](https://pub.dev/packages/flutter_redux) and tested with (flutter_redux_dev_tools)[https://pub.dev/packages/flutter_redux_dev_tools].

Thanks for [@pszklarska](https://github.com/pszklarska) for the great redux example [shopping_cart](https://github.com/pszklarska/flutter_shopping_cart).