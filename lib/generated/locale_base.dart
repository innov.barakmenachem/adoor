import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class LocaleBase {
  Map<String, dynamic> _data;
  String _path;
  Future<void> load(String path) async {
    _path = path;
    final strJson = await rootBundle.loadString(path);
    _data = jsonDecode(strJson);
    initAll();
  }
  
  Map<String, String> getData(String group) {
    return Map<String, String>.from(_data[group]);
  }

  String getPath() => _path;

  LocaleButtons _Buttons;
  LocaleButtons get Buttons => _Buttons;
  LocaleGateState _GateState;
  LocaleGateState get GateState => _GateState;
  LocaleMessages _Messages;
  LocaleMessages get Messages => _Messages;
  Localemain _main;
  Localemain get main => _main;

  void initAll() {
    _Buttons = LocaleButtons(Map<String, String>.from(_data['Buttons']));
    _GateState = LocaleGateState(Map<String, String>.from(_data['GateState']));
    _Messages = LocaleMessages(Map<String, String>.from(_data['Messages']));
    _main = Localemain(Map<String, String>.from(_data['main']));
  }
}

class LocaleButtons {
  final Map<String, String> _data;
  LocaleButtons(this._data);

  String get btn_open_to_truck => _data["btn_open_to_truck"];
  String get btn_open_gate_to_car => _data["btn_open_gate_to_car"];
}
class LocaleGateState {
  final Map<String, String> _data;
  LocaleGateState(this._data);

  String get OPEN => _data["OPEN"];
  String get OPENING => _data["OPENING"];
  String get CLOSE => _data["CLOSE"];
  String get CLOSING => _data["CLOSING"];
  String get MAKING_PHONE_CALL => _data["MAKING_PHONE_CALL"];
}
class LocaleMessages {
  final Map<String, String> _data;
  LocaleMessages(this._data);

  String get msg_current_gate_state => _data["msg_current_gate_state"];
}
class Localemain {
  final Map<String, String> _data;
  Localemain(this._data);

  String get sample => _data["sample"];
}
