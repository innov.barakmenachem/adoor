import 'package:adoor/generated/locale_base.dart';
import 'package:flutter/material.dart';

class LocDelegate extends LocalizationsDelegate<LocaleBase> {
  const LocDelegate();
  final idMap = const {'en': 'locales/EN_US.json', 'he': 'locales/HE_IS.json'};

  @override
  bool isSupported(Locale locale){
      return ['en', 'he'].contains(locale.languageCode);
  } 

  @override
  Future<LocaleBase> load(Locale locale) async {
    var lang = 'he';
    if (isSupported(locale)) {
      lang = locale.languageCode;
    }
    final loc = LocaleBase();
    await loc.load(idMap[lang]);
    return loc;
  }

  @override
  bool shouldReload(LocDelegate old) => false;
}




