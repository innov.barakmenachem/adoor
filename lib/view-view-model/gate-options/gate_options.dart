import 'package:adoor/assets/assets.dart';
import 'package:adoor/constants/constants.dart';
import 'package:adoor/generated/locale_base.dart';
import 'package:adoor/model/app_state.dart';
import 'package:adoor/model/gate_state_type.dart';
import 'package:adoor/model/open_gate_config.dart';
import 'package:adoor/redux/actions.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';

class AdoorGateOptions extends StatefulWidget {
  AdoorGateOptions({Key key}) : super(key: key);

  @override
  _AdoorGateOptionsState createState() => _AdoorGateOptionsState();
}

/*

 appBar: AppBar(
            title: Text('Adoor'),
            actions: <Widget>[
              Text(
                  "${loc.Messages.msg_current_gate_state}: ${viewModel.currentState.localToString(loc)}"),
            ],
          ),


Text("Current gate state: ${viewModel.currentState}"),
              RaisedButton(
                child: Text("Open To Truck"),
                onPressed: () {
                  viewModel.onOpenToTruck(GateOpenConfig("open to truck", 10));
                },
              )
*/
class _AdoorGateOptionsState extends State<AdoorGateOptions> {
  @override
  Widget build(BuildContext context) {
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return StoreConnector<AppState, AdoorGateOptionsViewModel>(
      converter: (store) => AdoorGateOptionsViewModel.build(store),
      builder: (context, viewModel) {
        return Scaffold(
            body: Stack(
          children: <Widget>[
            OptionsBackgound(),
            Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 80),
                        Container(width: 200, child: svg_logo),
                        Text(
                          "לחץ פעמיים להפעלה",
                          style: TextStyle(color: Colors.black45),
                        ),
                        SizedBox(height: 30),
                        InteractiveCard(height: 100, title: "פתח שער למקסימום"),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                                child: InteractiveCard(
                                    title: "פתח לאדם",
                                    height: 150,
                                    child: Column(
                                      children: <Widget>[
                                        Icon(
                                          Icons.person,
                                          color: AppColors.textColor,
                                        ),
                                        SizedBox(height: 10),
                                        Text("1 מטרים",
                                            style: TextStyle(
                                                color: AppColors.textColor)),
                                        SizedBox(height: 10),
                                      ],
                                    ))),
                            Expanded(
                                child: InteractiveCard(
                                    title: "פתח לרכב",
                                    height: 150,
                                    child: Column(
                                      children: <Widget>[
                                        Icon(
                                          Icons.directions_car,
                                          color: AppColors.textColor,
                                        ),
                                        SizedBox(height: 10),
                                        Text("3 מטרים",
                                            style: TextStyle(
                                                color: AppColors.textColor)),
                                        SizedBox(height: 10),
                                      ],
                                    ))),
                          ],
                        ),
                        InteractiveCard(height: 100, title: "סגור שער"),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                GateAnimationCard(),
              ],
            )
          ],
        ));
      },
    );
  }
}

class GateAnimationCard extends StatefulWidget {
  GateAnimationCard({Key key}) : super(key: key);

  @override
  _GateAnimationCardState createState() => _GateAnimationCardState();
}

class _GateAnimationCardState extends State<GateAnimationCard> {
  final FlareControls controls = FlareControls();
  bool isOpen = false;
   void _playOpenAnimation() {
    // Use the controls to trigger an animation.
    controls.play("Open1meter");
    isOpen = true;
  }

  void _playCloseAnimation() {
    // Use the controls to trigger an animation.
    controls.play("Close1meter");
    isOpen = false;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        width: double.infinity,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30)),
          ),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Text(
                "מצב השער",
                style: AppTextStyles.titleStyle,
              ),
              GestureDetector(
                onTap: () {
                  if (!isOpen) {
                    _playOpenAnimation();
                  } else {
                    _playCloseAnimation();
                  }
                },
                child: SizedBox(
                  height: 150,
                  width: 300,
                  child: FlareActor("lib/assets/gate.flr",
                      animation: "Idle",
                      fit: BoxFit.contain,
                      alignment: Alignment.center,
                      controller: controls),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class OptionsBackgound extends StatelessWidget {
  const OptionsBackgound({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: RadialGradient(
          center: const Alignment(0, -0.8), // near the top right
          radius: 0.5,
          colors: [
            AppColors.lightBrown,
            AppColors.brown,
          ],
          stops: [0.0, 30],
        ),
      ),
    );
  }
}

typedef OnOpenTo = Function(GateOpenConfig gateOpenConfig);

class AdoorGateOptionsViewModel {
  //state:
  final List<GateOpenConfig> openGateConfigs;
  final GateOpenConfig currentGateOpenConfig;
  final GateState currentState;

  //callbacks
  final OnOpenTo onOpenToTruck;

  AdoorGateOptionsViewModel(
      {this.openGateConfigs,
      this.currentGateOpenConfig,
      this.currentState,
      this.onOpenToTruck});

  static AdoorGateOptionsViewModel build(Store<AppState> store) {
    return AdoorGateOptionsViewModel(
      openGateConfigs: store.state.openGateConfigs,
      currentGateOpenConfig: store.state.currentGateOpenConfig,
      currentState: store.state.currentState,
      onOpenToTruck: (gateOpenConfig) {
        store.dispatch(SetOpenGateConfigAction(gateOpenConfig));
        store.dispatch(MakePhoneCallAction("+972524671405"));
      },
    );
  }
}

typedef OnDubleTap = Function();

class InteractiveCard extends StatefulWidget {
  final double width;
  final double height;
  final String title;
  final Widget child;
  final OnDubleTap onTap;

  InteractiveCard(
      {Key key,
      this.width,
      this.height = 50,
      this.title = "",
      this.child,
      this.onTap})
      : super(key: key);

  @override
  _InteractiveCardState createState() => _InteractiveCardState();
}

class _InteractiveCardState extends State<InteractiveCard> {
  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onDoubleTap: () {
        setState(() {
          isTapped = !isTapped;
        });
      },
      // When the child is tapped, show a snackbar.

      child: Container(
        width: widget.width,
        height: widget.height,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          color: isTapped
              ? AppColors.tappedCardBackground
              : AppColors.idleCardBackground,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  widget.title,
                  style: isTapped
                      ? AppTextStyles.smallTitleStyleWhite
                      : AppTextStyles.smallTitleStyle,
                ),
                widget.child == null
                    ? Container()
                    : Padding(
                        child: widget.child,
                        padding: EdgeInsets.only(top: 10),
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
