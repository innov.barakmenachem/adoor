import 'package:redux/redux.dart';
import 'package:adoor/assets/assets.dart';
import 'package:adoor/constants/constants.dart';
import 'package:adoor/model/app_state.dart';
import 'package:adoor/view-view-model/gate-options/gate_options.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:adoor/redux/actions.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[LoginBackgound(), LoginFront()],
    ));
  }
}

class LoginFront extends StatefulWidget {
  LoginFront({Key key}) : super(key: key);

  @override
  _LoginFrontState createState() => _LoginFrontState();
}

class _LoginFrontState extends State<LoginFront> {
  String currentText = "";

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, LoginModelViewModel>(
      converter: (store) => LoginModelViewModel.build(store),
      builder: (context, viewModel) {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              svg_logo,
              SizedBox(
                height: 30,
              ),
              Container(
                width: 350,
                height: 200,
                child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(35.0),
                    ),
                    color: AppColors.idleCardBackground,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 30),
                          Text(
                            "Enter Sign In Code",
                            style: AppTextStyles.titleStyle,
                          ),
                          SizedBox(height: 20),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: PinCodeTextField(
                              length: 5,
                              obsecureText: false,
                              backgroundColor: AppColors.idleCardBackground,
                              inactiveColor: Colors.blueGrey,
                              animationType: AnimationType.slide,
                              shape: PinCodeFieldShape.underline,
                              animationDuration: Duration(milliseconds: 200),
                              borderRadius: BorderRadius.circular(1),
                              onChanged: (value) {
                                setState(() {
                                  currentText = value;
                                });
                              },
                              onCompleted: (value) {
                                viewModel.onCodeEnter(value);
                              },
                            ),
                          )
                        ],
                      ),
                    )),
              )
            ],
          ),
        );
      },
    );
  }
}

class LoginBackgound extends StatelessWidget {
  const LoginBackgound({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: RadialGradient(
          center: const Alignment(0, -0.2), // near the top right
          radius: 0.6,
          colors: [
            AppColors.lightBrown,
            AppColors.brown,
          ],
          stops: [0.0, 15],
        ),
      ),
    );
  }
}

typedef OnTextEnter = Function(String text);

class LoginModelViewModel {
  //state:
  final bool isLoggedIn;
  final OnTextEnter onCodeEnter;

  LoginModelViewModel({this.isLoggedIn, this.onCodeEnter});

  static LoginModelViewModel build(Store<AppState> store) {
    return LoginModelViewModel(
      isLoggedIn: store.state.isLoggedIn,
      onCodeEnter: (code) {
        store.dispatch(CheckCodeAction(code));
      },
    );
  }
}
