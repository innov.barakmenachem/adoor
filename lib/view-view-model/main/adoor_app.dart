import 'package:adoor/generated/locale_base.dart';
import 'package:adoor/localization/localization.dart';
import 'package:adoor/model/app_state.dart';
import 'package:adoor/os-services/lifecycle_event_handler.dart';
import 'package:adoor/view-view-model/gate-options/gate_options.dart';
import 'package:adoor/view-view-model/login/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class AdoorApp extends StatefulWidget {
  final Store<AppState> store;
  const AdoorApp(this.store);

  @override
  _AdoorAppState createState() => _AdoorAppState();
}

class _AdoorAppState extends State<AdoorApp> {
  final LocDelegate localeOverrideDelegate = LocDelegate();

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: widget.store,
      child: MaterialApp(
        title: 'Adoor',
        localizationsDelegates: [
          localeOverrideDelegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          // const Locale('en', 'US'), // English
          const Locale('he', 'IL'), // Hebrew
        ],
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: StoreBuilder<AppState>(
            onInit: (store) => {},
            builder: (context, store) {
              return Adoor(store);
            }),
      ),
    );
  }
}

class Adoor extends StatefulWidget {
  final DevToolsStore<AppState> store;

  Adoor(this.store, {Key key}) : super(key: key);

  @override
  _AdoorState createState() => _AdoorState();
}

class _AdoorState extends State<Adoor> {
  final FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(
      LifecycleEventHandler(
        pausedCallBack: () {
          //todo: implement it
          print('PAUSE!!');
          return;
        },
      ),
    );
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return StoreConnector<AppState, LoginModelViewModel>(
      converter: (store) => LoginModelViewModel.build(store),
      builder: (context, viewModel) {
        return Scaffold(
          body: viewModel.isLoggedIn? AdoorGateOptions() : LoginPage(),//AdoorGateOptions()
        );
      },
    );
  }
}
