import 'package:flutter/material.dart';

class AppColors {
  static Color textColor = Color(0xFF6E6C68);
  static Color idleCardBackground = Color(0xFFF2F2F2);
  static Color tappedCardBackground = Color(0xFFFFB86C);
  static Color lightBrown = Color(0xFFDABA98);
  static Color brown = Color(0xFFA8866B);
}

class AppTextStyles {
  static TextStyle titleStyle = TextStyle(
    fontFamily: 'Roboto',
    fontSize: 24,
    fontWeight: FontWeight.bold,
    color: AppColors.textColor,
  );
  static TextStyle smallTitleStyle = TextStyle(
    fontFamily: 'Roboto',
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: AppColors.textColor,
  );
   static TextStyle smallTitleStyleWhite = TextStyle(
    fontFamily: 'Roboto',
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: Colors.white,
  );
}
