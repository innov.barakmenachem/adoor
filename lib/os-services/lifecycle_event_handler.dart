import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

/* Took from: https://stackoverflow.com/questions/49869873/flutter-update-widgets-on-resume
 * 
 * Use Example:
 * ```dart
 *    class AppWidgetState extends State<AppWidget> {
 *      void initState() {
 *        super.initState();
 *    
 *         WidgetsBinding.instance.addObserver(
 *               LifecycleEventHandler(
 *                 pausedCallBack: () {
 *                   print('PAUSE!!');
 *                   return;
 *                 },
 *               ),
 *             );
 *      }
 *      ...
 *    }
 * ```
 * 
 * 
 * More options to implement is with:
 * ```dart
 *      SystemChannels.lifecycle.setMessageHandler((msg) {
 *         print('SystemChannels> $msg');
 *         if (msg == AppLifecycleState.resumed.toString()) setState(() {});
 *      });
 * ```
 */
class LifecycleEventHandler extends WidgetsBindingObserver {
  final AsyncCallback inactiveCallBack;
  final AsyncCallback pausedCallBack;
  final AsyncCallback detachedCallBack;
  final AsyncCallback resumeCallBack;


  static Future<void> _defaultCallback(){
      var completer = new Completer<void>()..complete();
      return completer.future;
  }
  
     
  LifecycleEventHandler({
    this.inactiveCallBack = _defaultCallback,
    this.pausedCallBack = _defaultCallback,
    this.detachedCallBack = _defaultCallback,
    this.resumeCallBack = _defaultCallback,
  });

  @override
  Future<Null> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.inactive:
        await inactiveCallBack();
        break;
      case AppLifecycleState.paused:
        await pausedCallBack();
        break;
      case AppLifecycleState.detached:
        await detachedCallBack();
        break;
      case AppLifecycleState.resumed:
        await resumeCallBack();
        break;
    }
  }
}
