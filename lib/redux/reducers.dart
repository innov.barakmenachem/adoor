import 'package:adoor/model/app_state.dart';
import 'package:adoor/redux/actions.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';

/* get state and apply the action changes on it.
 * 
 * @param   state       current state.
 * @param   action      an 'ReducerAction', the action we want to use.
 */
AppState appStateReducers(AppState state, dynamic action) {
  if (action is! DevToolsAction) {
    return action.apply(state);
  } else {
    return state;
  }
}
