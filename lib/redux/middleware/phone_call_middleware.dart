import 'package:adoor/model/app_state.dart';
import 'package:adoor/redux/actions.dart';
import 'package:call_number/call_number.dart';
import 'package:redux/redux.dart';

class PhoneCallMiddleware extends MiddlewareClass<AppState> {
  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    if (action is MakePhoneCallAction) {
      _makePhoneCall(store, action);
    }

    next(action);
  }

  Future _makePhoneCall(
      Store<AppState> store, MakePhoneCallAction action) async{
    await new CallNumber().callNumber(action.phoneNumber).timeout(new Duration(seconds: 3));
    store.dispatch(PhoneCallComplete());
  }


}
