import 'package:adoor/model/app_state.dart';
import 'package:adoor/redux/actions.dart';
import 'package:redux/redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';

class LoggerMiddleware extends MiddlewareClass<AppState> {
  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    next(action);

    if (action is! DevToolsAction) {
      print('Logger - Action: $action');
      print('Logger - State: ${store.state.toJson()}');

    }
  }
}
