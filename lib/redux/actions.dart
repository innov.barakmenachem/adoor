import 'dart:convert';

import 'package:adoor/model/app_state.dart';
import 'package:adoor/model/gate_state_type.dart';
import 'package:adoor/model/open_gate_config.dart';
import 'package:crypto/crypto.dart';
import 'package:equatable/equatable.dart';

abstract class ReducerAction extends Equatable {
  // This class is intended to be used as a mixin, and should not be
  // extended directly.

  AppState apply(AppState state);
}

/* Setting the state of the gate(close -> openinig | opening -> open | etc.)
 * 
 */
class SetGateStateAction extends ReducerAction {
  final GateState gateState;

  SetGateStateAction(this.gateState);

  @override
  List<Object> get props => [gateState];

  @override
  AppState apply(AppState state) {
    return AppState.fromGateState(state, gateState);
  }
}

/* Setting the open config
 * @example   (opening for 10 seconds and then stop | opening for 1 second and then stop | etc.)
 * 
 */
class SetOpenGateConfigAction extends ReducerAction {
  final GateOpenConfig gateOpenConfig;

  SetOpenGateConfigAction(this.gateOpenConfig);

  @override
  List<Object> get props => [gateOpenConfig];

  @override
  AppState apply(AppState state) {
    return AppState.fromGateOpenConfig(state, gateOpenConfig);
  }
}

/* Adding option of opening the gate.
 * 
 */
class AddOpenGateConfigAction extends ReducerAction {
  final GateOpenConfig gateOpenConfig;

  AddOpenGateConfigAction(this.gateOpenConfig);

  @override
  List<Object> get props => [gateOpenConfig];

  @override
  AppState apply(AppState state) {
    return AppState.fromOpenGateConfigs(
      state,
      List.from(state.openGateConfigs)..add(gateOpenConfig),
    );
  }
}

/* Making phone call to a phone number.
 * 
 */
class MakePhoneCallAction extends ReducerAction {
  final String phoneNumber;

  MakePhoneCallAction(this.phoneNumber);

  @override
  List<Object> get props => [phoneNumber];

  @override
  AppState apply(AppState state) {
    if (state.currentState.state == GateStateType.OPEN) {
      return AppState.fromGateState(state, GateState(GateStateType.CLOSING));
    } else if (state.currentState.state == GateStateType.CLOSE) {
      return AppState.fromGateState(state, GateState(GateStateType.OPENING));
    }
    return state;
  }
}

/* replace the current state of the gate in any time that phone call is done.
 * its somthing like state machine of the gate.
 * 
 */
class PhoneCallComplete extends ReducerAction {
  PhoneCallComplete();

  @override
  List<Object> get props => [];

  @override
  AppState apply(AppState state) {
    if (state.currentState.state == GateStateType.OPENING) {
      return AppState.fromGateState(state, GateState(GateStateType.OPEN));
    } else if (state.currentState.state == GateStateType.CLOSING) {
      return AppState.fromGateState(state, GateState(GateStateType.CLOSE));
    }
    return state;
  }
}

class CheckCodeAction extends ReducerAction {
  final String code;

  CheckCodeAction(this.code);

  @override
  List<Object> get props => [code];

  @override
  AppState apply(AppState state) {
    var salt = 'UVocjgjgXg8P7zIsC93kKlRU8sPbTBhsAMFLnLUPDRYFIWAk';
    var validHash =
        'a478f93baa076c15dc0ba318d8bcd5de22c0be26a68c5d8850af69cde278a6b7';

    var saltedPassword = salt + code;
    var bytes = utf8.encode(saltedPassword);
    var hash = sha256.convert(bytes).toString();

    if (hash == validHash) {
      return AppState.fromIsLoggedIn(state, true);
    } else {
      return state;
    }
  }
}
