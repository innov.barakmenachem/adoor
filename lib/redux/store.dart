import 'package:adoor/model/app_state.dart';
import 'package:adoor/redux/middleware/logger_middleware.dart';
import 'package:adoor/redux/middleware/phone_call_middleware.dart';
import 'package:redux/redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';
import 'package:adoor/redux/reducers.dart';

Future<Store<AppState>> createReduxStore() async {
  //final apiClient = ApiClient();
  //final sharedPreferences = await SharedPreferences.getInstance();

  return DevToolsStore<AppState>(
    appStateReducers,
    initialState: AppState.empty(),
    middleware: [
      PhoneCallMiddleware(),
      LoggerMiddleware()
    ],
  );
}
