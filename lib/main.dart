import 'package:adoor/model/app_state.dart';
import 'package:adoor/redux/store.dart';
import 'package:adoor/view-view-model/main/adoor_app.dart';
import 'package:flutter/material.dart';


/* 
 * State managment is architecured with :
 * [flutter_redux](https://pub.dev/packages/flutter_redux) 
 * 
 * and tested with :
 * (flutter_redux_dev_tools)[https://pub.dev/packages/flutter_redux_dev_tools].
 * 
 * Thanks for:
 * [@pszklarska](https://github.com/pszklarska) 
 * 
 * for the great redux example:
 * [shopping_cart](https://github.com/pszklarska/flutter_shopping_cart).
 * 
 * 
 * created by Barak Menachem
*/
void main() async {
  final store = await createReduxStore();
  runApp(AdoorApp(store));
}
