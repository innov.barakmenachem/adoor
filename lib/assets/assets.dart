import 'package:adoor/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

final Widget svg_logo = SvgPicture.asset('lib/assets/adooraim-logo.svg', semanticsLabel: 'Adooraim Logo');

final Widget svg_car_icon = SvgPicture.asset('lib/assets/car-icon.svg', semanticsLabel: 'car-icon');
final Widget svg_person_icon = SvgPicture.asset('lib/assets/person-icon.svg', semanticsLabel: 'person-icon');

