import 'package:equatable/equatable.dart';

class GateOpenConfig extends Equatable {
  final String description;
  final int duration;

  GateOpenConfig(this.description, this.duration);

  GateOpenConfig.fromJson(Map<String, dynamic> json)
      : description = json['description'],
        duration = json['duration'];

  Map<String, dynamic> toJson() => {'description': description, 'duration': duration};

  @override
  String toString() {
    return "$description |||| $duration";
  }

  @override
  List<Object> get props => [description, duration];
}
