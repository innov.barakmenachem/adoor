import 'package:adoor/generated/locale_base.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

enum GateStateType { CLOSE, OPEN, OPENING, CLOSING, MAKING_PHONE_CALL }

class GateState extends Equatable {
  final GateStateType state;
  GateState(this.state);

  GateState.fromJson(Map<String, dynamic> json) : state = json['state'];

  Map<String, dynamic> toJson() => {'state': state};

  @override
  String toString() {
    return "$state";
  }

  String localToString(LocaleBase loc) {
    switch (state) {
      case GateStateType.OPEN:
        return "${loc.GateState.OPEN}";
        break;

      case GateStateType.OPENING:
        return "${loc.GateState.OPENING}";
        break;

      case GateStateType.CLOSE:
        return "${loc.GateState.CLOSE}";
        break;
      case GateStateType.CLOSING:
        return "${loc.GateState.CLOSING}";
        break;
      case GateStateType.MAKING_PHONE_CALL:
        return "${loc.GateState.MAKING_PHONE_CALL}";
        break;

      default:
        return "Unknown";
    }
  }

  @override
  List<Object> get props => [state];
}
