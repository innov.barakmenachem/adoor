import 'package:adoor/localization/localization.dart';
import 'package:adoor/model/gate_state_type.dart';
import 'package:adoor/model/open_gate_config.dart';
import 'package:equatable/equatable.dart';

class AppState extends Equatable {
  final List<GateOpenConfig> openGateConfigs;
  final GateOpenConfig currentGateOpenConfig;
  final GateState currentState;
  final LocDelegate localeOverrideDelegate;
  final bool isLoggedIn;

  AppState(
    this.localeOverrideDelegate,
    this.openGateConfigs,
    this.currentGateOpenConfig,
    this.currentState,
    this.isLoggedIn,
  );

  AppState.fromOpenGateConfigs(
      AppState state, List<GateOpenConfig> newOpenGateConfigs)
      : localeOverrideDelegate = state.localeOverrideDelegate,
        openGateConfigs = newOpenGateConfigs,
        currentGateOpenConfig = state.currentGateOpenConfig,
        currentState = state.currentState,
        isLoggedIn = state.isLoggedIn;

  AppState.fromGateOpenConfig(AppState state, GateOpenConfig newOpenGateConfig)
      : localeOverrideDelegate = state.localeOverrideDelegate,
        openGateConfigs = state.openGateConfigs,
        currentGateOpenConfig = newOpenGateConfig,
        currentState = state.currentState,
        isLoggedIn = state.isLoggedIn;

  AppState.fromGateState(AppState state, GateState newGateState)
      : localeOverrideDelegate = state.localeOverrideDelegate,
        openGateConfigs = state.openGateConfigs,
        currentGateOpenConfig = state.currentGateOpenConfig,
        currentState = newGateState,
        isLoggedIn = state.isLoggedIn;

  AppState.fromIsLoggedIn(AppState state, bool newIsLoggedIn)
      : localeOverrideDelegate = state.localeOverrideDelegate,
        openGateConfigs = state.openGateConfigs,
        currentGateOpenConfig = state.currentGateOpenConfig,
        currentState = state.currentState,
        isLoggedIn = newIsLoggedIn;

  factory AppState.fromJson(Map<String, dynamic> json) {
    return AppState(
        (json['localeOverrideDelegate'] as LocDelegate),
        (json['openGateConfigs'] as List)
            ?.map((i) => new GateOpenConfig.fromJson(i as Map<String, dynamic>))
            ?.toList(),
        (json['currentGateOpenConfig'] as GateOpenConfig),
        (json['currentState'] as GateState),
        (json['isLoggedIn'] as bool));
  }
  factory AppState.empty() => AppState(
        LocDelegate(),
        new List(),
        GateOpenConfig("no open config", 0),
        GateState(GateStateType.CLOSE),
        false,
      );

  Map<String, dynamic> toJson() => {
        'localeOverrideDelegate': localeOverrideDelegate,
        'openGateConfigs': openGateConfigs,
        'currentGateOpenConfig': currentGateOpenConfig,
        'currentState': currentState,
        'isLoggedIn': isLoggedIn,
      };

  @override
  String toString() =>
      "$openGateConfigs |||| $currentGateOpenConfig |||| $currentState |||| $isLoggedIn";

  @override
  List<Object> get props => [
        localeOverrideDelegate,
        openGateConfigs,
        currentGateOpenConfig,
        currentState,
        isLoggedIn
      ];
}
